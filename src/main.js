import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import ArgonDashboard from './plugins/argon-dashboard'
import VueLocalStorage from 'vue-localstorage'

Vue.config.productionTip = false
Vue.use(VueLocalStorage)
const moment = require('moment')
require('moment/locale/ru')

Vue.use(ArgonDashboard)
Vue.use(require('vue-moment'), {
  moment
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
