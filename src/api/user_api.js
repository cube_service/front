import BaseApi from './base'

class UserApi extends BaseApi {
    login(login, password){
      return this.api_client.post('/accounts/login/', {'email': login, 'password': password})
    }

    registration(login, password, password_double){
      return this.api_client.post('/accounts/registration/', {'email': login, 'password': password, 'password1': password_double})
    }

    resetPasswordRequestToken(login){
      return this.api_client.post('/accounts/password_reset/', {'email': login})
    }

    resetPasswordConfirmToken(token, new_password){
      return this.api_client.post('/accounts/password_reset/confirm/', {'token': token, 'password': new_password})
    }

    changePassword(old_password, new_password, new_password_1){
      return this.api_client.post('/accounts/change_password/', {'old_password': old_password, 'new_password': new_password, 'new_password_1': new_password_1})
    }

    getUserInfo(){
      return this.api_client.get('/accounts/')
    }
    
}
export default UserApi;
