import axios from 'axios'
import store from '../store/index'

class BaseApi {
    constructor(app_context){
        this.app = app_context
        this.errors = {}
        this.config = {
            baseURL: 'http://0.0.0.0:8000/api/',
            timeout: 5000
        }
        this.api_client = axios.create(this.config)
        let token = this.app.$localStorage.get('token')
        if (token) {
            console.log(token)
            this.api_client.defaults.headers.common['Authorization'] = 'Token ' + token
        }
        let api_class = this
        this.api_client.interceptors.response.use(
            function (response) {
                return response;
            },
            function(error) {
                api_class.errorResponseHandler(error, api_class)
            }
          )

    }

    errorResponseHandler(error, api) {

        if(error.config.hasOwnProperty('errorHandle') && error.config.errorHandle === false ) {
            return Promise.reject(error);
        }

        if (error.response) {
            
            switch(error.response.status) {
                case 401:
                    api.errors.connect_errors = ['Для совершения операции необходима авторизация']
                    break
                case 403:
                    api.errors.connect_errors = ['Нет прав для совершения операции']
                    break
                case 400:
                    api.errors.field_errors = error.response.data 
                    break
                default:
                    api.errors.connect_errors = ['Ой! Кажется что то пошло не так, напишите нам, и мы обязательно исправим это!']
                    break
            }
        }
        else{
            api.errors.connect_errors = ['Хмм. Кажется проблемы с подключением к нашему серверу, проверьте интернет соединение, если подключение в порядке напишите нам!']
        }
    }

    handleErrorMessages(ui_field_errors){
        if('connect_errors' in this.errors){
            store.commit('errorState/showErrorMessage', {'title': 'Ошибка', 'messages': this.errors.connect_errors});
            return
        }
        if('field_errors' in this.errors){
            if('non_field_errors' in this.errors.field_errors){
                let error_message = ''
                if(typeof this.errors.field_errors.non_field_errors === 'object'){
                    error_message = this.errors.field_errors.non_field_errors
                }else{
                    error_message = [this.errors.field_errors.non_field_errors]
                }
                delete this.errors.field_errors.non_field_errors
                store.commit('errorState/showErrorMessage', {'title': 'Ошибка', 'messages': error_message});
    
            }
            if(ui_field_errors && typeof ui_field_errors === 'object'){
                for (let field in ui_field_errors) {
    
                    if(field in this.errors.field_errors){
                        let error_message = this.errors.field_errors[field]
    
                        if(typeof error_message === 'object'){
                            let errors_text = ''
                            for(let error_text_index in error_message){
                                if (error_text_index > 0)
                                    errors_text += ' ' + error_message[error_text_index]
                                else{
                                    errors_text += error_message[error_text_index]
                                } 
                            }
                            ui_field_errors[field] = errors_text
                        }else{
                            ui_field_errors[field] = error_message
                        }
                        
                    }
                }
            }
        }
        
    }
}

export default BaseApi;
