import Vue from 'vue'
import Router from 'vue-router'
import DashboardLayout from '@/layout/DashboardLayout'
import AuthLayout from '@/layout/AuthLayout'
Vue.use(Router)

const router = new Router({
  linkExactActiveClass: 'active',
  routes: [
    {
      path: '/',
      redirect: 'products',
      component: DashboardLayout,
    },
    {
      path: '/products',
      component: DashboardLayout,
      children: [
        {
          path: '',
          component: () => import('./views/Products/ProductsList.vue'),
          meta: { 
            requiresAuth: true
          }
        },
        {
          path: 'detail',
          component: () => import('./views/Products/ProductDetail.vue')
        },
      ]
    },
    {
      path: '/sources',
      component: DashboardLayout,
      children: [
        {
          path: '',
          component: () => import('./views/Sources/List.vue')
        },
        {
          path: 'add',
          component: () => import('./views/Sources/Create.vue')
        },
        {
          path: 'detail',
          component: () => import('./views/Sources/Detail.vue')
        },
      ]
    },
    {
      path: '/stores',
      component: DashboardLayout,
      children: [
        {
          path: '',
          component: () => import('./views/Stores/List.vue')
        },
        {
          path: 'detail',
          component: () => import('./views/Stores/Detail.vue')
        }
      ]
    },
    {
      path: '/logs',
      component: DashboardLayout,
      children: [
        {
          path: '',
          component: () => import('./views/Logs/List.vue')
        }
      ]
    },
    {
      path: '/profile',
      component: DashboardLayout,
      children: [
        {
          path: '',
          component: () => import('./views/Profile.vue'),
          meta: { 
            requiresAuth: true
          }
        },
        {
          path: 'change_password',
          component: () => import('./views/ChangePassword.vue'),
          meta: { 
            requiresAuth: true
          }
        }
      ]
    },
    {
      path: '/notifications',
      component: DashboardLayout,
      children: [
        {
          path: '',
          component: () => import('./views/Notifications.vue')
        }
      ]
    },
    {
      path: '/feedback',
      component: DashboardLayout,
      children: [
        {
          path: '',
          component: () => import('./views/FeedBack.vue')
        }
      ]
    },
    {
      path: '/login',
      component: AuthLayout,
      children: [
        {
          path: '',
          component: () => import('./views/Login.vue')
        }
      ]
    },
    {
      path: '/registration',
      component: AuthLayout,
      children: [
        {
          path: '',
          component: () => import('./views/Registration.vue')
        }
      ]
    },
    {
      path: '/restore_password',
      component: AuthLayout,
      children: [
        {
          path: '',
          component: () => import('./views/RestorePassword.vue')
        }
      ]
    },
    {
      path: '/restore_password_confirm',
      component: AuthLayout,
      children: [
        {
          path: '',
          component: () => import('./views/RestorePasswordConfirm.vue')
        }
      ]
    },
  ]
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if (router.app.$localStorage.get('token', '') !== '') {
      next()
      return
    }
    next('/login') 
  } else {
    next() 
  }
})

export default router