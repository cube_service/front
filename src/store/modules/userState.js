const state = {
    name: '',
    token: ''
  };
  
const mutations = {
  
  setApiKey(state, key){
      state.token = key;
  },
  setUserName(state, name){
      state.name = name;
  },
  
};
  
export default {
  namespaced: true,
  state,
  mutations
}