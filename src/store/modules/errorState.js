const state = {
    error_title: '',
    error_messages: [],
    visible_error_modal: false
  };
  
const mutations = {
  
    showErrorMessage(state, error_data){
      state.error_title = error_data.title
      state.error_messages = error_data.messages
      state.visible_error_modal = true
    },
  
    hideErrorMessage(state){
      state.error_title = ''
      state.error_messages = []
      state.visible_error_modal = false
    }
};
  
export default {
  namespaced: true,
  state,
  mutations
}
  