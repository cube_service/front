const state = {
    visibleLoadSpinner: false,
  };
  
const mutations = {
  
  showSpinner(state){
      state.visibleLoadSpinner = true
  },
  hideSpinner(state){
    state.visibleLoadSpinner = false
  },
  
};
  
export default {
  namespaced: true,
  state,
  mutations
}