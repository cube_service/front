import Vue from 'vue'
import Vuex from 'vuex'
import errorState from './modules/errorState'
import userState from './modules/userState'
import appState from './modules/appState'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    errorState,
    userState,
    appState
  }
})
